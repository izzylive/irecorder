package ex.irec;

import android.app.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;

import static ex.irec.scheduler.SyncService.getMyJson;
/**
 * Created by milosz.szarek on 22.12.2017.
 */

public class WebViewFragment extends Fragment {

    WebView myWebView;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.webview_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        myWebView = (WebView) getActivity().findViewById(R.id.webView1);

        myWebView.getSettings().setJavaScriptEnabled(true);
        myWebView.addJavascriptInterface(new WebAppInterface(this.getActivity()),"Android");
        myWebView.setWebViewClient(new WebViewClient());

        ImageButton fab = (ImageButton) getActivity().findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getMyJson("/data/data/th.dtv/dtv_user_data/myfi.json");
                Snackbar.make(view, "Wybrane filmy zapamiętane, rekomendacje uaktualnią się po ostatnim nagraniu", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        //myWebView.loadUrl("http://filmywtelewizji.pl/indx.html");
    }

    @Override
    public void onStart() {
        super.onStart();
        String url = this.getArguments().getString("url");
        if (url != null) {
            loadUrl(url);
        }
    }

    public void loadUrl(String url) {
        if (url != null) {
            myWebView.loadUrl(url);
        }
    }


}
