package ex.irec;

import android.content.Context;

import android.util.Log;
import android.webkit.JavascriptInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;


import static android.content.ContentValues.TAG;
import static ex.irec.MainActivity.executeCommand;


@SuppressWarnings("ALL")
public class WebAppInterface {


    Context mContext;
    ArrayList<HashMap<String, String>> film_list = new ArrayList<>();

    /** Instantiate the interface and set the context */
    WebAppInterface(Context c) {
        mContext = c;
    }

    /** Show a toast from the web page */
    @JavascriptInterface
    public void addClick(String book_line) {

        HashMap<String, String> film = new HashMap<>();
        String Start, a;

        // adding each child node to HashMap key => value
        String str[]= book_line.split("__");  //split to array of params

        film.put("Id", str[0]);
        film.put("Channel", str[1]);
        film.put("Title", str[2]);
        film.put("Start", str[3]);
        film.put("Stop", str[4]);

        Log.d(TAG, "addClick: film"+ film);
        createMyFiFile(film);
        //Toast.makeText(mContext,"Ch: "+str[0]+"____"+str[1]+"-yr:"+yr+";"+mnth+";"+dy+"JDY: "+a, Toast.LENGTH_SHORT).show();
    }

    private void createMyFiFile(HashMap<String, String> filmAdd) {
    int Id_prev=0;
    int Id_actu=0;
    int Id_prevprev = 0;

   // if (film_list.size()==0) {}
    film_list = readMyFiFile();
    film_list.add(filmAdd);

        Collections.sort(film_list, new Comparator<HashMap<String, String>>() {
            @Override
            public int compare(final HashMap<String, String> o1, final HashMap<String, String> o2) {
                //  sorting...
                return Integer.valueOf(o1.get("Id"))
                        .compareTo(Integer.valueOf(o2.get("Id")));
            }
        });
        Log.d(TAG, "createMyFiFile: "+film_list);
         // remove double click
        int count = 0;
        while (film_list.size() > count) {
            Id_actu = Integer.valueOf(film_list.get(count).get("Id"));
            if (Id_actu == Id_prev) {
                film_list.remove(count);
                film_list.remove((count-1));
                Id_prev = Id_prevprev;          //correct Id previous value
            } else {
                Id_prev = Id_actu;              //save Id previous value
            }
            count++;
            Id_prevprev = Id_prev;
        }

        JSONArray json_arr = new JSONArray();
        for (HashMap<String, String> hm : film_list) {
            JSONObject json_obj = new JSONObject();
                try {
                    json_obj.put("Id", hm.get("Id"));
                    json_obj.put("Channel", hm.get("Channel"));
                    json_obj.put("Title", hm.get("Title")); //json_obj.put("RecDay", hm.get("RecDay"));
                    json_obj.put("Start", hm.get("Start"));
                    json_obj.put("Stop", hm.get("Stop"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            json_arr.put(json_obj);
        }
        String jsonstringout = json_arr.toString();
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream("/data/data/th.dtv/dtv_user_data/myfi.json"));
            outputStreamWriter.write(jsonstringout);
            outputStreamWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        executeCommand("chmod 777 /data/data/th.dtv/dtv_user_data/myfi.json");
    }

    private ArrayList<HashMap<String,String>> readMyFiFile() {

        String jsoninput;
        ArrayList<HashMap<String, String>> filmlist = new ArrayList<>();

        File file = new File("/data/data/th.dtv/dtv_user_data/myfi.json"); //do testów najlepsze 5
        StringBuilder x = null;
        int bookings_count;

        try {                                                   // read f5.json
            FileInputStream iStream = new FileInputStream(file);
            byte[] data = new byte[iStream.available()];
            x = new StringBuilder();
            int numRead = 0;
            while ((numRead = iStream.read(data)) >= 0) {
                x.append(new String(data, 0, numRead));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        jsoninput = String.valueOf(x); // zamiana na String
        String cid,cchannel,ctitle, cstart,cstop;

        try {                                                   // decode json
            JSONArray jsonArr = new JSONArray(jsoninput);
            bookings_count = jsonArr.length();

            for (int i = 0; i < bookings_count; i++) {
                JSONObject c = jsonArr.getJSONObject(i);
                cid = c.getString("Id");
                cchannel = c.getString("Channel");
                ctitle = c.getString("Title");
                cstart = c.getString("Start");
                cstop = c.getString("Stop");
                // tmp hash map for single film
                HashMap<String, String> film = new HashMap<>();

                // adding each child node to HashMap key => value
                film.put("Id", cid);
                film.put("Channel", cchannel);
                film.put("Title", ctitle);
                film.put("Start", cstart);
                film.put("Stop", cstop);
                // adding film to the list
                filmlist.add(film);
            }
        } catch (JSONException e) {
            //Log.e( " TAG JSONException => ", e);
            e.printStackTrace();
        }
        return filmlist;
    }

    public static int julianDay(int year, int month, int day) {
        int a = (14 - month) / 12;
        int y = year + 4800 - a;
        int m = month + 12 * a - 3;
        int jdn = day + (153 * m + 2)/5 + 365*y + y/4 - y/100 + y/400 - 32045;
        return jdn;
    }

    public static void createT1file(ArrayList<HashMap<String, String>> filmlst) {

        int bookings_count =filmlst.size();
        byte[] b1_bookings = new byte[1040];
        String cch_search,hid,hchannel,htitle, sstart,sstop;
        Short  hrecday, hstart, hstop, hduration;
        Integer cmuxadr;
        ByteArrayOutputStream baos = null;
        DataOutputStream dos = null;
        // create byte array output stream
        baos = new ByteArrayOutputStream();
        // create data output stream
        dos = new DataOutputStream(baos);
        byte[] b_title;
        int b_title_len;
        byte[] bin_startduration = {0,0,0,0};

        try {

            int j = 0;
            for (HashMap<String, String> hm : filmlst) {
                j++;
                hid = hm.get("Id");
                hchannel = hm.get("Channel").trim();
                htitle = hm.get("Title");
                b_title = convert(htitle,"UTF-8","ISO-8859-2");
                b_title_len = b_title.length;

                sstart = hm.get("Start"); //"20171230005500 +0100"
                sstop = hm.get("Stop");

                int yr = Integer.parseInt(sstart.substring(0,4));
                int mnth = Integer.parseInt(sstart.substring(4,6));
                int dy = Integer.parseInt(sstart.substring(6,8));
                hrecday = (short)( julianDay(yr,mnth,dy)-2400001);

                bin_startduration = startduration(sstart,sstop);
                //zmienic short na byte
                dos.write((byte)hchannel.length());
                dos.writeBytes(hchannel);
                //short na byte+3bytes+b_title
                dos.write((byte)(b_title_len+3)); //length+bytes 0x100002 (3b)
                dos.write(0x10);
                dos.write(0x0);
                dos.write(0x2);
                dos.write(b_title,0,b_title_len);
                //line for MuxAdr call prep
                cch_search = hchannel.replaceAll("\\s+", ""); //trim()?
                cmuxadr = myMuxAdress(cch_search);
                dos.writeInt(cmuxadr);
                dos.writeInt(0x00002268);
                dos.writeShort(hrecday);
                dos.write(bin_startduration);
                dos.write(0x0);
                dos.write(0x2); //book_type - 0none,1play,2record,3standby,4poweron
                dos.write(0x0); // book_mode 0 - once/2/weekly/3monthly 4 workday/5 weekend
                dos.write(0x0); //freq - 4b -4x00
                dos.write(0x0); //freq - 4b -4x00
                dos.write(0x0); //freq - 4b -4x00
                dos.write(0x0); //freq - 4b -4x00
                dos.flush();
                //Toast.makeText(this, "J:" + j + "Channel: " + cch_search + "  "+cmuxadr, Toast.LENGTH_LONG).show();
            }
        }catch (Exception e) {
            e.printStackTrace();
        } //finally

        int b_max=0;
        for (byte b:baos.toByteArray()) {
            b1_bookings[b_max]=b;
            b_max++;
        }
        //b_max=b_max-1;
        // Toast.makeText(this, "b_bookings:"+ (char)b_bookings[23]+ (char)b_bookings[41], Toast.LENGTH_LONG).show();
        //int max = dos.size();  baos.write(b_bookings,0,max);
        byte[] b_bookings = Arrays.copyOf(b1_bookings,b_max);
        byte[] bin_bookings = new byte[b_max+8];
        // header preparation
        int hdr1; // 4b byte_count - 4
        int hdr2; //4b crc32
        int hdr3 = 0x0006; //4b 5 for KIplus; 6 for KIpro
        int hdr4 = bookings_count;  //4b book_count

        int vInt = hdr3;
        byte[] bin_hdr3 = new byte[] {(byte)(vInt >>> 24),(byte)(vInt >>> 16),(byte)(vInt >>> 8),(byte)vInt};
        vInt = hdr4;
        byte[] bin_hdr4 = new byte[] {(byte)(vInt >>> 24),(byte)(vInt >>> 16),(byte)(vInt >>> 8),(byte)vInt};

        byte[] bin_hdr34 = new byte[8];

        bin_hdr34= join(bin_hdr3,bin_hdr4);  // hdr1, hdr2 calculated later
        bin_bookings = join (bin_hdr34,b_bookings); //hdr3+hdr4+b_bookings - for crc32 calculation

        hdr1 = b_max+12;
        hdr2 = myCrc32(bin_bookings);//bin_bookings
        vInt = hdr1;                        // convert from int to bytes
        byte[] bin_hdr1 = new byte[] {(byte)(vInt >>> 24),(byte)(vInt >>> 16),(byte)(vInt >>> 8),(byte)vInt};
        vInt = hdr2;                        // convert from int to bytes
        byte[] bin_hdr2 = new byte[] {(byte)(vInt >>> 24),(byte)(vInt >>> 16),(byte)(vInt >>> 8),(byte)vInt};
        byte[] bin_hdr12 = join(bin_hdr1,bin_hdr2);
        byte[] bin_record = join(bin_hdr12,bin_bookings); //all bytes

        try {
            OutputStream outb = new FileOutputStream("/data/data/th.dtv/dtv_user_data/dtv_mw_t1");
            outb.write(bin_record,0,b_max+16);  //baos.size+16b for header ZMIENIC na niepuste?

        } catch (IOException e) {
            e.printStackTrace();
        }
        executeCommand("chmod 777 /data/data/th.dtv/dtv_user_data/dtv_mw_t1");

        //dispme=(TextView)findViewById(R.id.textView4);
        //dispme.setText("Json: "+"title: ");
        //Toast.makeText(this, "1: "+hdr1+"2: "+hdr2, Toast.LENGTH_LONG).show();
    }

    public static Integer myCrc32(byte[] bytesseq){


        // CRC32 lookup table for polynomial 0x04c11db7
        int[] crc_table = {
                0x00000000, 0x04c11db7, 0x09823b6e, 0x0d4326d9, 0x130476dc, 0x17c56b6b,
                0x1a864db2, 0x1e475005, 0x2608edb8, 0x22c9f00f, 0x2f8ad6d6, 0x2b4bcb61,
                0x350c9b64, 0x31cd86d3, 0x3c8ea00a, 0x384fbdbd, 0x4c11db70, 0x48d0c6c7,
                0x4593e01e, 0x4152fda9, 0x5f15adac, 0x5bd4b01b, 0x569796c2, 0x52568b75,
                0x6a1936c8, 0x6ed82b7f, 0x639b0da6, 0x675a1011, 0x791d4014, 0x7ddc5da3,
                0x709f7b7a, 0x745e66cd, 0x9823b6e0, 0x9ce2ab57, 0x91a18d8e, 0x95609039,
                0x8b27c03c, 0x8fe6dd8b, 0x82a5fb52, 0x8664e6e5, 0xbe2b5b58, 0xbaea46ef,
                0xb7a96036, 0xb3687d81, 0xad2f2d84, 0xa9ee3033, 0xa4ad16ea, 0xa06c0b5d,
                0xd4326d90, 0xd0f37027, 0xddb056fe, 0xd9714b49, 0xc7361b4c, 0xc3f706fb,
                0xceb42022, 0xca753d95, 0xf23a8028, 0xf6fb9d9f, 0xfbb8bb46, 0xff79a6f1,
                0xe13ef6f4, 0xe5ffeb43, 0xe8bccd9a, 0xec7dd02d, 0x34867077, 0x30476dc0,
                0x3d044b19, 0x39c556ae, 0x278206ab, 0x23431b1c, 0x2e003dc5, 0x2ac12072,
                0x128e9dcf, 0x164f8078, 0x1b0ca6a1, 0x1fcdbb16, 0x018aeb13, 0x054bf6a4,
                0x0808d07d, 0x0cc9cdca, 0x7897ab07, 0x7c56b6b0, 0x71159069, 0x75d48dde,
                0x6b93dddb, 0x6f52c06c, 0x6211e6b5, 0x66d0fb02, 0x5e9f46bf, 0x5a5e5b08,
                0x571d7dd1, 0x53dc6066, 0x4d9b3063, 0x495a2dd4, 0x44190b0d, 0x40d816ba,
                0xaca5c697, 0xa864db20, 0xa527fdf9, 0xa1e6e04e, 0xbfa1b04b, 0xbb60adfc,
                0xb6238b25, 0xb2e29692, 0x8aad2b2f, 0x8e6c3698, 0x832f1041, 0x87ee0df6,
                0x99a95df3, 0x9d684044, 0x902b669d, 0x94ea7b2a, 0xe0b41de7, 0xe4750050,
                0xe9362689, 0xedf73b3e, 0xf3b06b3b, 0xf771768c, 0xfa325055, 0xfef34de2,
                0xc6bcf05f, 0xc27dede8, 0xcf3ecb31, 0xcbffd686, 0xd5b88683, 0xd1799b34,
                0xdc3abded, 0xd8fba05a, 0x690ce0ee, 0x6dcdfd59, 0x608edb80, 0x644fc637,
                0x7a089632, 0x7ec98b85, 0x738aad5c, 0x774bb0eb, 0x4f040d56, 0x4bc510e1,
                0x46863638, 0x42472b8f, 0x5c007b8a, 0x58c1663d, 0x558240e4, 0x51435d53,
                0x251d3b9e, 0x21dc2629, 0x2c9f00f0, 0x285e1d47, 0x36194d42, 0x32d850f5,
                0x3f9b762c, 0x3b5a6b9b, 0x0315d626, 0x07d4cb91, 0x0a97ed48, 0x0e56f0ff,
                0x1011a0fa, 0x14d0bd4d, 0x19939b94, 0x1d528623, 0xf12f560e, 0xf5ee4bb9,
                0xf8ad6d60, 0xfc6c70d7, 0xe22b20d2, 0xe6ea3d65, 0xeba91bbc, 0xef68060b,
                0xd727bbb6, 0xd3e6a601, 0xdea580d8, 0xda649d6f, 0xc423cd6a, 0xc0e2d0dd,
                0xcda1f604, 0xc960ebb3, 0xbd3e8d7e, 0xb9ff90c9, 0xb4bcb610, 0xb07daba7,
                0xae3afba2, 0xaafbe615, 0xa7b8c0cc, 0xa379dd7b, 0x9b3660c6, 0x9ff77d71,
                0x92b45ba8, 0x9675461f, 0x8832161a, 0x8cf30bad, 0x81b02d74, 0x857130c3,
                0x5d8a9099, 0x594b8d2e, 0x5408abf7, 0x50c9b640, 0x4e8ee645, 0x4a4ffbf2,
                0x470cdd2b, 0x43cdc09c, 0x7b827d21, 0x7f436096, 0x7200464f, 0x76c15bf8,
                0x68860bfd, 0x6c47164a, 0x61043093, 0x65c52d24, 0x119b4be9, 0x155a565e,
                0x18197087, 0x1cd86d30, 0x029f3d35, 0x065e2082, 0x0b1d065b, 0x0fdc1bec,
                0x3793a651, 0x3352bbe6, 0x3e119d3f, 0x3ad08088, 0x2497d08d, 0x2056cd3a,
                0x2d15ebe3, 0x29d4f654, 0xc5a92679, 0xc1683bce, 0xcc2b1d17, 0xc8ea00a0,
                0xd6ad50a5, 0xd26c4d12, 0xdf2f6bcb, 0xdbee767c, 0xe3a1cbc1, 0xe760d676,
                0xea23f0af, 0xeee2ed18, 0xf0a5bd1d, 0xf464a0aa, 0xf9278673, 0xfde69bc4,
                0x89b8fd09, 0x8d79e0be, 0x803ac667, 0x84fbdbd0, 0x9abc8bd5, 0x9e7d9662,
                0x933eb0bb, 0x97ffad0c, 0xafb010b1, 0xab710d06, 0xa6322bdf, 0xa2f33668,
                0xbcb4666d, 0xb8757bda, 0xb5365d03, 0xb1f740b4
        };
        // zamiast linijki poniżej zamiana str_in na bytes
        //byte [] bytes = { 0x0, 0x0, 0x0, 0x5, 0x0, 0x0, 0x0, 0x0 };
        int crc = 0xffffffff; int x =0;
        for (byte b:bytesseq) { //bytesseq
            crc = (crc << 8) ^ crc_table[((crc >> 24) ^ b) & 0xff];
            //Log.i(TAG,x+"::"+b);//           x++;
        }return crc; //16605015 == 00FD5F57
    }

    private static Integer myMuxAdress(String Channel) {
        Integer MuxAdr;
        // decoder based in one location, add read dtv_mw_s1 and create your own muxCh/muxAdr array

        String[] muxCh = {"TVP1","TVP2","Katowice","Kraków","TVPKultura","TVPHistoria","TVPRozrywka","TVPInfo",
                "Polsat","TVN","TV4","TVPuls","TVN7","PULS2","TV6","SuperPolsat",
                "8TV","TTV","Tvtrwam","TVPABC","Stopklatka","FocusTV","test1","test2",
                "test3","Metro","Zoom","Nowa","WP1","TVRepublika"};

        int[] muxAdr = {0x00010003,0x00020003,0x000B0003,0x000D0003,0x001F0003,0x00200003,0x00220003,0x00230003,
                0x00030002,0x00040002,0x00050002,0x00060002,0x00170002,0x00180002,0x00190002,0x001A0002,
                0x001B0001,0x001C0001,0x00320001,0x00330001,0x00340001,0x00350001,0x00360001,0x00370001,
                0x00380001,0x00390001,0x003A0001,0x003B0001,0x003C0001,0x005C0001};

        int max = muxCh.length; int x =0;

        for (int i = 0; i < max; i++) {

            if (Channel.equals(muxCh[i])) {
                //Toast.makeText(this, "I:" + i + "muxCh: " + muxCh[i] + "Channel: "+Channel+"fAdr: "+muxAdr[i], Toast.LENGTH_LONG).show();

                return muxAdr[i];
            }
        }
        return -1;
    }

    private static byte[] join(byte[] a, byte[] b) {
        // join two byte arrays
        final byte[] ret = new byte[a.length + b.length];
        System.arraycopy(a, 0, ret, 0, a.length);
        System.arraycopy(b, 0, ret, a.length, b.length);
        return ret;
    }

    public static byte[] convert(String mystring, String setFrom, String setTo) {
        //ByteBuffer outputBuffer;
        Charset charsetFrom = Charset.forName(setFrom);
        Charset charsetTo = Charset.forName(setTo);

        byte[] bb = mystring.getBytes();
        ByteBuffer inputBuffer = ByteBuffer.wrap(bb);

// decode UTF-8
        CharBuffer data = charsetFrom.decode(inputBuffer);

// encode ISO-8559-1
        ByteBuffer outputBuffer = charsetTo.encode(data);
        byte [] output = outputBuffer.array();
        return output;

    }

    private static byte[] startduration (String dateStart, String dateStop) {

        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
        SimpleDateFormat formatter = new SimpleDateFormat("HHmm");

        //calculate duration and convert to byte array
        Date d1 = null;
        Date d2 = null;

        try {
            d1 = format.parse(dateStart);
            d2 = format.parse(dateStop);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long duration = (d2.getTime() - d1.getTime());
        long Min = (duration / (60 * 1000) % 60);
        long Hrs = (duration / (60 * 60 * 1000));

        //"20171230005500 +0100" - 1h mniej czas zimowy
        String dStart=formatter.format((d1.getTime()-60*60*1000));// zmiana 0 1 godz czas zimowy
        String sduration = String.format("%02d", Hrs)+String.format("%02d", Min);

        //convert start:HH:mm to byte array
        byte[] b = hex2ByteArray(dStart+sduration);


        int aaa=0;
    return b;
    }

    public static byte[] hex2ByteArray(String s) {
        try {

            int len = s.length();
            if(len>1) {
                byte[] data = new byte[len / 2];
                for (int i = 0 ; i < len ; i += 2) {
                    data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                            + Character.digit(s.charAt(i + 1), 16));
                }
                return data;
            }
            else
            {
                return  null;
            }
        } catch (Exception e) {
            throw e;
        }
    }

}