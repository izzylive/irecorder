package ex.irec;


import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;
import android.preference.PreferenceManager;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;

import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;



import ex.irec.scheduler.SyncScheduler;

@SuppressWarnings("ALL")
public class MainActivity extends AppCompatActivity {
    private static final String TAG = "Act:";
    private static final String DTV_APP_CLASSPATH = "th.dtv";
    TextView dispme;
    String url_add;

    WebViewFragment webViewFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        install();
        SyncScheduler scheduler = new SyncScheduler(getApplicationContext());
        SharedPreferences prefs =
                PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        String syncInterval = prefs.getString("sync_frequency", "-1");

        if (syncInterval.equals("-1")){
            scheduler.unschedule();
        }else{
            scheduler.schedule(Integer.parseInt(syncInterval));
        }

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        webViewFragment = new WebViewFragment();
        url_add = jsonVisible("/data/data/th.dtv/dtv_user_data/myfi.json");
        loadWebView("http://filmywtelewizji.pl/indx.html?id_js="+url_add);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        String FirstKeyTmp;

        if (id == R.id.films_R) {
            setTitle("Najlepsze Filmy");
            url_add = jsonVisible("/data/data/th.dtv/dtv_user_data/myfi.json");
            loadWebView("http://filmywtelewizji.pl/indx.html?id_js="+url_add);
            return true;
        }
        if (id == R.id.films_T) {
            setTitle("Najwcześniejsze Filmy");
            url_add = jsonVisible("/data/data/th.dtv/dtv_user_data/myfi.json");
            loadWebView("http://filmywtelewizji.pl/wkdy.html?id_js="+url_add);
            return true;
        }
        if (id == R.id.films_S) {
            setTitle("Seriale w Telewizji");
            //url_add dodać gdy gotowe
            loadWebView("http://filmywtelewizji.pl/sers.html");
            return true;
        }
        if (id == R.id.myfilms) {
            setTitle("Mój Wybór");
            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            String nick = sharedPref.getString("example_text", "Not known to us").toString();
            loadWebView("http://filmywtelewizji.pl/test1.html");
            return true;
        }
        if (id == R.id.about) {          // informacyjne
            Toast.makeText(this, R.string.about_toast, Toast.LENGTH_LONG).show();
            return true;
        }
        if (id == R.id.set) {
            Intent modifySettings = new Intent(MainActivity.this, SettingsActivity.class);
            startActivity(modifySettings);
            //dispme=(TextView)findViewById(R.id.textView4);
            //dispme.setText("Nothing is selected");
            return true;
        }
        if (id == R.id.exit) {
            askForFinish();
        }
        return (super.onOptionsItemSelected(item));
    }

    private String jsonVisible(String filename) {
        //read f10.json and create url_add=url_ids - list of bookmarked films

        String jsoninput, cid;
        File file = new File(filename); //do testów najlepsze 5
        StringBuilder x = null;
        StringBuilder y = null;
        int bookings_count;

        try {                                                   // read myfi.json
            FileInputStream iStream = new FileInputStream(file);
            byte[] data = new byte[iStream.available()];
            x = new StringBuilder();
            x.append("");
            int numRead = 0;
            while ((numRead = iStream.read(data)) >= 0) {
                x.append(new String(data, 0, numRead));
            }
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
        jsoninput = String.valueOf(x); // zamiana na String

        try {                                                   // decode json
            JSONArray jsonArr = new JSONArray(jsoninput);
            bookings_count = jsonArr.length();
            y= new StringBuilder();
            y.append("");
            for (int i = 0; i < bookings_count; i++) {
                JSONObject c = jsonArr.getJSONObject(i);
                cid = c.getString("Id");
                y.append( cid+";");
            }
        } catch (JSONException e) {
            //Log.e( " TAG JSONException => ", e);
            e.printStackTrace();
        }
        String url_ids = y.toString();
        return url_ids;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    if (webViewFragment.myWebView.canGoBack()) {
                        webViewFragment.myWebView.goBack();
                    } else {
                        askForFinish();
                    }
                    return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    public void askForFinish(){
        AlertDialog.Builder builder = new AlertDialog.Builder(
                new ContextThemeWrapper(this, R.style.dialogAlertTheme)
        );
        builder.setMessage("Wyjście z aplikacji?")
                .setCancelable(false)
                .setPositiveButton("Tak", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        finish();
                    }
                })
                .setNegativeButton("Nie", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                })
                .setNeutralButton("DTV", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        finishApp();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void finishApp(){
        Intent launchIntent = getPackageManager().getLaunchIntentForPackage(DTV_APP_CLASSPATH);
        if (launchIntent != null) {
            startActivity(launchIntent);//null pointer check in case package name was not found
        }
    }

    public void loadWebView(String url){
        if (findViewById(R.id.fragment_container) != null) {
            // However, if we're being restored from a previous state, then we don't need to do anything
            // and should return or else  we could end up with overlapping fragments.
            //if (savedInstanceState != null) {     return;   }

            if (webViewFragment.isVisible()){
                webViewFragment.loadUrl(url);
            }else{
                Bundle args = new Bundle();
                args.putString("url",url);
                webViewFragment.setArguments(args);
                getFragmentManager().beginTransaction().add(R.id.fragment_container, webViewFragment).commit();
                getFragmentManager().executePendingTransactions();
            }
        }
    }

    public void install(){
        Process p = null;
        try {
            p = Runtime.getRuntime().exec("su");
            DataOutputStream dos = new DataOutputStream(p.getOutputStream());
            dos.writeBytes("chmod 777 /data/data/th.dtv/dtv_user_data\n");
            dos.writeBytes("chmod 755 /data/data/th.dtv/dtv_user_data/dtv_mw_s1\n");
            dos.writeBytes("exit\n");
            dos.flush();
            dos.close();
            p.waitFor();
        } catch (Exception e) {
            Log.e(TAG,"Installation failed: ");
        }
    }

    public static String executeCommand(String command) {
        BufferedReader reader = null;
        String result = "";
        try {
            Process p = Runtime.getRuntime().exec(command);            //execute
            reader = new BufferedReader(new InputStreamReader(p.getInputStream())); //read output of the command
            String line = null;
            while ((line = reader.readLine()) != null) {
                result += line + "\n";
            }
            p.waitFor();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (reader != null)
                try {
                    reader.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
        }
        Log.e("TAG,result: ", result);
        return result;
    }

}



