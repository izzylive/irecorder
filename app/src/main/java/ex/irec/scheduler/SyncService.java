package ex.irec.scheduler;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;

import ex.irec.R;
import static ex.irec.WebAppInterface.createT1file;
import static ex.irec.MainActivity.executeCommand;

public class SyncService extends IntentService {

    private String T1RemoteFilePath = "/www/dvb/f10.json";
    private String T1LocalFilePath = "/data/data/th.dtv/dtv_user_data/f10.json";

    private String S1RemoteFilePath = "/www/dvb/s1_milosz__o2.pl";
    private String S1LocalFilePath = "/data/data/th.dtv/dtv_user_data/dtv_mw_s1";

    private String ftpHost = "ftp.webserwer.pl";
    private String ftpUser = "assistance48";
    private String ftpPassword = "12ala34";
    private int ftpDefaultTimeout = 10 * 1000; // 10 seconds

    public SyncService() {
        super("SyncService");
    }

    public SyncService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d(SyncScheduler.tag, "SyncService intent handler");
        job();
        SharedPreferences prefs =
                PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        String syncInterval = prefs.getString("sync_frequency", "-1");
        if (syncInterval.equals("-1")){
            return;
        }else{
            getMyJson("/data/data/th.dtv/dtv_user_data/f10.json");
        }

    }

    private void job(){
        Log.i(SyncScheduler.tag, "Sync job start");
        try {

            // download T1 to temp file
            File outputDir = getApplicationContext().getCacheDir();
            File tmpT1File = File.createTempFile("T1SYNC", "", outputDir);
            Log.d(SyncScheduler.tag, "Downloading T1 from " + T1RemoteFilePath + " to " + tmpT1File.getAbsolutePath());
            OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(
                    tmpT1File));
            boolean downloaded = getT1(T1RemoteFilePath, outputStream);
            outputStream.close();
            if (downloaded){
                executeCommand("chmod 777 " + tmpT1File.getAbsolutePath());
                // rename to target path
                File T1TargetFile = new File(T1LocalFilePath);
                boolean renamed = tmpT1File.renameTo(T1TargetFile);
                Log.d(SyncScheduler.tag, "Moving T1 File from " + tmpT1File.getAbsolutePath() + " to " + T1LocalFilePath + ": " + String.valueOf(renamed));
            }
            // upload S1
            Log.d(SyncScheduler.tag, "Uploading S1 from " + S1LocalFilePath + " to " + S1RemoteFilePath);
            File inputFile = new File(S1LocalFilePath);
            InputStream inputStream = new FileInputStream(inputFile);
            boolean putS1success = putS1(S1RemoteFilePath, inputStream);
            Log.i(SyncScheduler.tag, "Sync job end");

        }catch(Exception e){
            Log.e(SyncScheduler.tag, "Sync job failed", e);
        }
    }

    private boolean getT1(String remoteFilePath, OutputStream outputStream) {
        boolean success = false;
        boolean status = false;
        FTPClient mFtpClient = null;
        try {
            mFtpClient = new FTPClient();
            mFtpClient.setConnectTimeout(ftpDefaultTimeout);
            mFtpClient.setDefaultTimeout(ftpDefaultTimeout);
            mFtpClient.connect(ftpHost);
            status = mFtpClient.login(ftpUser, ftpPassword);
            Log.d(SyncScheduler.tag, "FTP Connected: " + String.valueOf(status));
            if (FTPReply.isPositiveCompletion(mFtpClient.getReplyCode())) {
                mFtpClient.enterLocalPassiveMode();
                mFtpClient.setFileType(FTP.BINARY_FILE_TYPE);
                success = mFtpClient.retrieveFile(remoteFilePath, outputStream);
                Log.d(SyncScheduler.tag, "File downloaded: " + String.valueOf(success));
            }
        } catch (java.net.ConnectException e){
            Log.d(SyncScheduler.tag, "Downloading T1 failed", e);
        } catch (java.net.SocketTimeoutException e){
            Log.d(SyncScheduler.tag, "Downloading T1 failed", e);
        } catch (Exception e) {
            Log.e(SyncScheduler.tag, "Downloading T1 failed", e);
        } finally {
                try {
                    mFtpClient.disconnect();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
        return success;
    }

    private boolean putS1(String remoteFilePath, InputStream inputStream) {
        boolean success = false;
        boolean status = false;
        FTPClient mFtpClient = null;
        try {
            mFtpClient = new FTPClient();
            mFtpClient.setConnectTimeout(ftpDefaultTimeout);
            mFtpClient.setDefaultTimeout(ftpDefaultTimeout);
            mFtpClient.connect(ftpHost);
            status = mFtpClient.login(ftpUser, ftpPassword);
            Log.d(SyncScheduler.tag, "FTP Connected: " + String.valueOf(status));
            if (FTPReply.isPositiveCompletion(mFtpClient.getReplyCode())) {
                mFtpClient.enterLocalPassiveMode();
                mFtpClient.setFileType(FTP.BINARY_FILE_TYPE);
                success = mFtpClient.storeFile(remoteFilePath, inputStream);
                Log.d(SyncScheduler.tag, "File uploaded: " + String.valueOf(success));
            }
        } catch (java.net.ConnectException e){
            Log.d(SyncScheduler.tag, "Uploading S1 failed: ", e);
        } catch (java.net.SocketTimeoutException e){
            Log.d(SyncScheduler.tag, "Uploading S1 failed", e);
        } catch (Exception e) {
            Log.e(SyncScheduler.tag, "Uploading S1 failed", e);
        } finally {
            try {
                mFtpClient.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return success;
    }

    public static void getMyJson(String filename) {
        //wczytuje f10.json i wywołuje createT1file

        String jsoninput;
        ArrayList<HashMap<String, String>> filmlist = new ArrayList<>();

        File file = new File(filename); //do testów najlepsze 5
        StringBuilder x = null;
        int bookings_count;

        try {                                                   // read f5.json
            FileInputStream iStream = new FileInputStream(file);
            byte[] data = new byte[iStream.available()];
            x = new StringBuilder();
            int numRead = 0;
            while ((numRead = iStream.read(data)) >= 0) {
                x.append(new String(data, 0, numRead));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        jsoninput = String.valueOf(x); // zamiana na String
        String cid,cchannel,ctitle, cstart,cstop;


        try {                                                   // decode json
            JSONArray jsonArr = new JSONArray(jsoninput);
            bookings_count = jsonArr.length();

            for (int i = 0; i < bookings_count; i++) {
                JSONObject c = jsonArr.getJSONObject(i);
                cid = c.getString("Id");
                cchannel = c.getString("Channel");
                ctitle = c.getString("Title");
                cstart = c.getString("Start");
                cstop = c.getString("Stop");
                // tmp hash map for single film
                HashMap<String, String> film = new HashMap<>();

                // adding each child node to HashMap key => value
                film.put("Id", cid);
                film.put("Channel", cchannel);
                film.put("Title", ctitle);
                film.put("Start", cstart);
                film.put("Stop", cstop);
                // adding film to the list
                filmlist.add(film);
            }
        } catch (JSONException e) {
            //Log.e( " TAG JSONException => ", e);
            e.printStackTrace();
        }
        createT1file(filmlist);
    }

}



