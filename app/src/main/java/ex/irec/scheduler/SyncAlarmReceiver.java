package ex.irec.scheduler;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class SyncAlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(SyncScheduler.tag, "SyncAlarmReceiver onReceive" );
        Intent inService = new Intent(context, SyncService.class);
        context.startService(inService);
    }
}

