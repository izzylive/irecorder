package ex.irec.scheduler;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import java.util.Calendar;

public class SyncScheduler {

    public static final String tag = "SCHEDULER";

    private AlarmManager alarmMgr;
    private PendingIntent alarmIntent;

    public SyncScheduler(Context context){
        // TODO: get interval from preferences sync_frequency
        //SharedPreferences prefs =  context.getApplicationContext().getSharedPreferences("ex.irecorder_preferences", Context.MODE_PRIVATE);
        alarmMgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, SyncAlarmReceiver.class);
        alarmIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
    }

    public void schedule(int syncInterval){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                syncInterval * 60 * 1000, alarmIntent);
    }

    public void unschedule(){
        if (alarmMgr!= null) {
            alarmMgr.cancel(alarmIntent);
        }
    }


}
