package ex.irec.scheduler;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

public class BootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            Log.d(SyncScheduler.tag, "BootReceiver onReceive" );
            SyncScheduler scheduler = new SyncScheduler(context);
            SharedPreferences prefs =
                    PreferenceManager.getDefaultSharedPreferences(context);
            String syncInterval = prefs.getString("sync_frequency", "-1");

            if (syncInterval.equals("-1")){
                scheduler.unschedule();
            }else{
                scheduler.schedule(Integer.parseInt(syncInterval));
            }
        }
    }
    /**
     * You may want to test boot manually:
     * adb shell am broadcast -a android.intent.action.BOOT_COMPLETED -p ex.irecorder
     */
}
